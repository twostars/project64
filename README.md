Project64
=========

Fork of official Project64 repository (http://www.pj64-emu.com:8090/project64.development/) with various bits of cleanup and robustness.

Build Notes
-----------
Builds only in x86 (i.e. 32 bit).

Builds with VS 2013 Community Edition.
